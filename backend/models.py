# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from backend.emails import send_email
# Create your models here.



# Class model for an email
class Email(models.Model):
    identity = models.BigAutoField(primary_key=True, editable=False)
    sendername = models.CharField(verbose_name="Sender Name", max_length=100, blank=True, null=True)
    email = models.CharField(verbose_name="Email", max_length=200, blank=True, null=True)
    source = models.CharField(verbose_name="Source", max_length=200, blank=True, null=True)
    message = models.TextField(verbose_name="Message", blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return "%s"%self.message


    def save(self, *args, **kwargs):
        send_email([settings.ADMIN_EMAIL, ], 'Email Received', self, settings.ADMIN_STATIC_URL)

        super(Transaction, self).save(*args, **kwargs)
