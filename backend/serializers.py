from rest_framework import serializers
from backend.models import Email
from django.db import transaction
import datetime
import json
from django.conf import settings
from rest_framework.exceptions import APIException
from django.db import  transaction

#serializer of an email
class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = '__all__'
