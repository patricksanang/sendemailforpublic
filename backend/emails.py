# app/emails.py
from django.conf import settings
from django.core.mail import send_mail, EmailMessage
from django.template.loader import render_to_string

def _send_email(to_list, subject, message, sender):
    msg = EmailMessage(subject=subject, body=message, from_email=sender, bcc=to_list)
    msg.content_subtype = "html"  # Main content is now text/html
    return msg.send()


def send_email(list, subject, email, static_url):
    msg_html = render_to_string('backend/email.html', {'source': email.source, 'sender': email.sendername, 'message': email.message, 'email': email.email, 'date': email.created})
    _send_email(to_list=list, subject=subject, message=msg_html, sender=settings.ADMIN_EMAIL)
