from backend import views
from rest_framework.routers import DefaultRouter
from django.conf.urls import url, include
from backend import views
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.authtoken.views import obtain_auth_token
from django.views.decorators.csrf import csrf_exempt

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'email', views.EmailViewSet)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
