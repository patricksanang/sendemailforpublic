# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from backend.models import Email
from backend.serializers import EmailSerializer
from rest_framework import viewsets, permissions
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from django.db import  transaction
# Create your views here.

#viewset of an email
class EmailViewSet(viewsets.ModelViewSet):
    queryset = Email.objects.all()
    serializer_class = EmailSerializer
    #permission_classes = (IsAuthenticated,)
    #authentication_classes = (TokenAuthentication,)
