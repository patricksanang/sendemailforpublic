This project is a simple which consists to expose a simple api to send contact emails from any source.

To deploy, you need to set your own in settings.py:
1. email host
2. email port
3. email host user
4. email host password

After that, you have to run: 

1. python manage.py makemigrations 
2. python manage.py migrate
3. python manage.py runserver

Then you can access to the api to send email with the end point : http://127.0.0.1:8080/api/email

To send an email, you just have to send a post request to the end point above with the following parameters:

- sendername : the name of the sender
- email : the email of the sender
- source : the source from which the email is sent
- message : the message to be sent in the email
